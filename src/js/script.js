import {
    addRemoveTransitionBodyResize,
    media,
    bodyScroll,
} from './function';


//------------------------------------------------------ SCRIPTS -----------------------------------------------------//
$(document).ready(function() {
    $('.menu-button').on('click', function() {
        let $regionHeaderRight = $('.region-header-right');
        let $mobileMenu = $('.mobile-menu');
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $mobileMenu.addClass('active');
            bodyScroll('.region-mobile-menu');
        } else {
            if($regionHeaderRight.hasClass('active') && $mobileMenu.hasClass('active')) {
                $regionHeaderRight.removeClass('active');
                $(this).removeClass('-search -visible');
            }else if($regionHeaderRight.hasClass('active')) {
                $regionHeaderRight.removeClass('active');
                $(this).removeClass('active -search -visible');
            }else {
                $mobileMenu.removeClass('active');
                $(this).removeClass('active');
                bodyScroll();
            }
        }
    });

    $('.search-button').on('click', function() {
        $('.region-header-right').addClass('active');

        setTimeout(function() {
            $('.menu-button').addClass('active -search');
        },400);

        setTimeout(function() {
            $('.menu-button').addClass('-visible');
        },500);
    });


    $(document).on("click", (function(e) {
        let $regionHeaderRight = $('.region-header-right');

        if ($regionHeaderRight.hasClass('active') && media(1199)) {
            $(e.target).closest('.region-header-right, .search-button, .mobile-menu').length
            || $($regionHeaderRight).removeClass('active')
            && $('.menu-button').removeClass('active -search -visible');
        }
    }));

    // //----------------------------------------------------------------------------------------------------------------//
    // //---------------------------------------------------- Resize ----------------------------------------------------//

    $(window).on('resize', function() {
        // отключаем  transition при resize и возврращаем через 700мс после resize -------------------------------------
        addRemoveTransitionBodyResize();

        // При открытом мобильном меню в зависимости от разрешения убираем scroll body ---------------------------------
        let $activeMobileMenu = $('.mobile-menu.active');
        if(media(1200, 'min') && $activeMobileMenu.length) {
            bodyScroll();
        }else if(media(1199, 'max') && $activeMobileMenu.length) {
            bodyScroll('.region-mobile-menu');
        }
    });
});

//----------------------------------------------------- FUNCTION -----------------------------------------------------//
