import {disableBodyScroll, clearAllBodyScrollLocks} from 'body-scroll-lock';

// Отключаем  transition  ----------------------------------------------------------------------------------------------
export function addRemoveTransitionBodyResize() {
    $('html').addClass('transition');
    setTimeout(function() {
        $('html').removeClass('transition');
    }, 700);
}//конец скрипта--------------------------------------------------------------------------------------------------------

// отключаем или включаем скролл у body --------------------------------------------------------------------------------
export function bodyScroll(elem) {
    if(elem){
        disableBodyScroll(document.querySelector(elem));
    }else {
        clearAllBodyScrollLocks();
    }
}

// медиа запрос --------------------------------------------------------------------------------------------------------
export const media = (param, minMax = 'max') => {
    return window.matchMedia(`(${minMax}-width: ${param}px)`).matches;
};